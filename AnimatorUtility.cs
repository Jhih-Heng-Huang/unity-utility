using System.Collections;
using UnityEngine;

public static class AnimatorUtility
{
	public static IEnumerator WaitAnimationEnd (this Animator animator, string name)
	{
		while (!animator.GetCurrentAnimatorStateInfo (0).IsName (name)) {
			yield return null;
		}

		if (animator.GetCurrentAnimatorStateInfo(0).loop)
			while (animator.GetCurrentAnimatorStateInfo(0).IsName(name)) yield return null;
		else
			while (animator.GetCurrentAnimatorStateInfo (0).IsName (name) && animator.GetCurrentAnimatorStateInfo (0).normalizedTime < 1.0f) {
				yield return null;
			}
	}

	public static bool IsStateName(this Animator animator, string name)
	{
		return animator.GetCurrentAnimatorStateInfo (0).IsName (name);
	}
}