using System;
using System.Collections;
using System.Collections.Generic;
using Rayark.Mast;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Nrjwolf.Tools.AttachAttributes;

namespace UnityUtility
{
	public static class GameobjectExtensions
	{
		public static T Instantiate2D<T>(this T original, Transform parent, Vector2 position) where T: MonoBehaviour
		{
			var obj = GameObject.Instantiate<T>(original, Vector3.zero, Quaternion.identity, parent);
			obj.GetComponent<RectTransform>().anchoredPosition = position;
			return obj;
		}
	}
}
