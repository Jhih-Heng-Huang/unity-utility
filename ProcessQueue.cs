using System.Collections;
using System.Collections.Generic;

namespace LittleFighterII.Engine
{
	public class ProcessQueue
	{
		private Queue<IEnumerator> _queue = new Queue<IEnumerator>();
		private bool _IsRunning = false;
		private bool _isStopped = false;

		public void Enqueue(IEnumerator process)
		{
			_queue.Enqueue(process);
		}

		public IEnumerator Run()
		{
			if (_IsRunning) yield break;

			_IsRunning = true;
			while (!_isStopped)
			{
				if (_queue.Count == 0)
					yield return null;
				else
				{
					var process = _queue.Dequeue();
					yield return process;
				}
			}
			_IsRunning = false;
			_isStopped = false;
		}

		public void Stop()
		=> _isStopped = true;

		public void Clear()
		=> _queue.Clear();
	}
}